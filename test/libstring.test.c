#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libtest/libtest.h>
#include "libstring.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
void test_trim();
void test_split();
void test_ends_with();
void test_starts_with();
void test_index();
void test_last_index();
void test_string_append();
void test_substring();
void test_lowercase();
void test_uppercase();
void test_to_bool();

/* Global Variables ***********************************************************/

int main(int argc, char *argv[]) {
  test_init("libstring", argc, argv);

  test("Trim", test_trim);
  test("Split", test_split);
  test("Ends With", test_ends_with);
  test("Starts With", test_starts_with);
  test("Index Of", test_index);
  test("Last Index Of", test_last_index);
  test("Append", test_string_append);
  test("Substring", test_substring);
  test("Lowercase", test_lowercase);
  test("Uppercase", test_uppercase);
  test("To Boolean", test_to_bool);

  return test_complete();
}

void test_trim() {
  char value1[] = "   value";
  char value2[] = "value   ";
  char value3[] = "   value   ";
  char value4[] = "hello   world";
  char value5[] = "\nvalue\n";

  test_assert_string(trim(&value1), "value", "Should trim spaces from the start");
  test_assert_string(trim(&value2), "value", "Should trim spaces from the end");
  test_assert_string(trim(&value3), "value", "Should trim spaces from the start and end");
  test_assert_string(trim(&value4), "hello   world", "Should not remove spaces between words");
  test_assert_string(trim(&value5), "value", "Should trim new lines");
}

void test_split() {
  test_assert_string(split("hello world", " "), "hello", "Should return up to the first delimiter");

  test_assert_string(split("abc,123", ","), "abc", "Should be able to use different delimiters");
  test_assert_string(strtok(NULL, ","), "123", "Should be able to use strtok() to retrieve the next value");
}

void test_ends_with() {
  test_assert(ends_with("myfile.txt", ".txt"), "Should return true when substring is at the end");
  test_assert(!ends_with("myfile.txt.gz", ".txt"), "Should return false when substring is not at the end");

  test_assert(ends_with("c", "c"), "Should handle single character strings");
}

void test_starts_with() {
  test_assert(starts_with("library.", "library.libtest"), "Should return true when substring is at the start");
  test_assert(!starts_with("library.", "author"), "Should return false when substring is at the start");

  test_assert(starts_with("l", "l"), "Should handle single character strings");
}

void test_index() {
  test_assert_int( string_index("some.thing", '.'), 4, "Should return correct index" );
  test_assert_int( string_index(".some", '.'), 0, "Should handle first index" );
  test_assert_int( string_index("thing.", '.'), 5, "Should handle last index" );
  test_assert_int( string_index("something", '.'), -1, "Should return -1 when character not found" );
}

void test_last_index() {
  test_assert_int( string_last_index("something", '.'), -1, "Should return -1 when character not found" );
  test_assert_int( string_last_index("some.thing", '.'), 4, "Should handle a single character found" );
  test_assert_int( string_last_index("file-1.0.0.so", '.'), 10, "Should return the index of the last character found" );
}

void test_string_append() {
  test_assert_string( string_append("hello", "world"), "helloworld", "Should join the strings together" );
}

void test_substring() {
  test_assert_string( substring("some.thing", 0, 3), "some", "Should substring from the start" );
  test_assert_string( substring("some.thing", 5, 9), "thing", "Should substring from the end" );
  test_assert_string( substring("some.value.thing", 5, 9), "value", "Should substring from the middle" );
  test_assert_string( substring("some.thing", 5, -1), "thing", "Should default end to length of string when -1" );
  test_assert_string( substring("some.thing", 3, 2), "", "Should return an empty string if start is greater than end" );
  test_assert_string( substring("some.thing", 20, 5), "", "Should return an empty string if the start exceeds the length of the string" );
  test_assert_string( substring("some.thing", 2, 20), "", "Should return an empty string if the end exceeds the length of the string" );
}

void test_lowercase() {
  test_assert_string( lowercase("HELLOWORLD"), "helloworld", "Should convert uppercase to lowercase" );
  test_assert_string( lowercase("helloworld"), "helloworld", "Should not covert lowercase" );
  test_assert_string( lowercase("123"), "123", "Should not convert numbers" );
  test_assert_string( lowercase("+_-"), "+_-", "Should not convert symbols" );
}

void test_uppercase() {
  test_assert_string( uppercase("helloworld"), "HELLOWORLD", "Should covert lowercase to uppercase" );
  test_assert_string( uppercase("HELLOWORLD"), "HELLOWORLD", "Should not convert uppercase" );
  test_assert_string( uppercase("123"), "123", "Should not convert numbers" );
  test_assert_string( uppercase("+_-"), "+_-", "Should not convert symbols" );
}

void test_to_bool() {
  test_assert( string_to_bool("true"), "Should return 'true' for 'true'" );
  test_assert( string_to_bool("yes"), "Should return 'true' for 'yes'" );
  test_assert( string_to_bool("on"), "Should return 'true' for 'on'" );
  test_assert( string_to_bool("y"), "Should return 'true' for 'y'" );
  test_assert( string_to_bool("1"), "Should return 'true' for '1'" );

  test_assert( !string_to_bool("false"), "Should return 'false' for 'false'" );
  test_assert( !string_to_bool("no"), "Should return 'false' for 'no'" );
  test_assert( !string_to_bool("off"), "Should return 'false' for 'off'" );
  test_assert( !string_to_bool("n"), "Should return 'false' for 'n'" );
  test_assert( !string_to_bool("0"), "Should return 'false' for '0'" );

  test_assert( !string_to_bool("wubalubadubdub"), "Should return 'false' for random strings" );
}
