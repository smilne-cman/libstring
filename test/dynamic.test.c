#include <stdio.h>
#include <stdlib.h>
#include <libtest/libtest.h>
#include "dynamic.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static void test_constructor();
static void test_trim();
static void test_prepend();
static void test_append();
static void test_find();
static void test_replace();
static void test_splice();
static void test_lowercase();
static void test_uppercase();
static void test_to_string();
static void test_equals();

/* Global Variables ***********************************************************/

int main(int argc, char *argv[]) {
  test_init("Dynamic Strings", argc, argv);

  test("Initialisation", test_constructor);
  test("Trim", test_trim);
  test("Prepend", test_prepend);
  test("Append", test_append);
  test("Find", test_find);
  test("Replace", test_replace);
  test("Splice", test_splice);
  test("Lowercase", test_lowercase);
  test("Uppercase", test_uppercase);
  test("To String", test_to_string);
  test("Equals", test_equals);

  return test_complete();
}

static void test_constructor() {
  DynamicString *string = dynamic_string("hello world");

  test_assert_int(string->length, 11, "Should set the length");
  test_assert_string(string->value, "hello world", "Should set the value");

  string->destroy(string);
}

static void test_trim() {
  DynamicString *string = dynamic_string("  hello  world  \n");

  string->trim(string);
  test_assert_string(string->value, "hello  world", "Should trim leading and trailing spaces and new lines");

  string->destroy(string);
}

static void test_prepend() {
  DynamicString *string = dynamic_string("world");

  string->prepend(string, "hello ");
  test_assert_string(string->value, "hello world", "Should add to the start of the string");

  string->destroy(string);
}

static void test_append() {
  DynamicString *string = dynamic_string("hello");

  string->append(string, " world");
  test_assert_string(string->value, "hello world", "Should add to the start of the string");

  string->destroy(string);
}

static void test_find() {
  DynamicString *string = dynamic_string("wubalubadubdub");

  test_assert_int(string->find(string, "lub"), 4, "Should return the index if found");
  test_assert_int(string->find(string, "blah"), -1, "Should return -1 if not found");
  test_assert_int(string->find(string, "dubstep"), -1, "Should ignore partial matches");

  string->destroy(string);
}

static void test_replace() {
  DynamicString *string = dynamic_string("Hello {name}!{name}");

  string->replace(string, "{name}", "Steven");
  test_assert_string(string->value, "Hello Steven!{name}", "Should replace a single instance of the target");

  string->replace(string, "{age}", "HELLO");
  test_assert_string(string->value, "Hello Steven!{name}", "Should not replace unmatched targets");

  string->replace(string, "{names}", "HELLO");
  test_assert_string(string->value, "Hello Steven!{name}", "Should not replace partial matches");

  string->replace(string, "{name}", "");
  test_assert_string(string->value, "Hello Steven!", "Should be able to cut text");

  string->destroy(string);
}

static void test_splice() {
  DynamicString *string = dynamic_string("ABC abc 123");

  string->splice(string, 3, 7, NULL);
  test_assert_string(string->value, "ABC 123", "Should splice characters out of the string");

  string->splice(string, 3, 3, " abc");
  test_assert_string(string->value, "ABC abc 123", "Should inject characters into the string");

  string->splice(string, 4, 7, "XYZ");
  test_assert_string(string->value, "ABC XYZ 123", "Should splice characters out and inject characters into the string");

  string->splice(string, 0, string->length, "Something else");
  test_assert_string(string->value, "Something else", "Should be able to splice the entire string");
}

static void test_lowercase() {
  DynamicString *string = dynamic_string("HELLO world!");

  string->lowercase(string);
  test_assert_string(string->value, "hello world!", "Should convert all characters to lowercase");
}

static void test_uppercase() {
  DynamicString *string = dynamic_string("HELLO world!");

  string->uppercase(string);
  test_assert_string(string->value, "HELLO WORLD!", "Should convert all characters to uppercase");
}

static void test_to_string() {
  DynamicString *string = dynamic_string("hello world");

  const char *value = string->to_string(string);
  test_assert_string(value, "hello world", "Should return the value as a string");

  string->assign(string, "goodbye world");
  test_assert_string(value, "hello world", "Should not be affected by future edits");

  string->destroy(string);
}

static void test_equals() {
  DynamicString *string1 = dynamic_string("hello");
  DynamicString *string2 = dynamic_string("hello");
  DynamicString *string3 = dynamic_string("world");

  test_assert(string1->equals(string1, string1), "Should equal itself");
  test_assert(string1->equals(string1, string2), "Should equal string with same value");
  test_assert(!string1->equals(string1, string3), "Should not equal string with different value");

  string1->destroy(string1);
  string2->destroy(string2);
  string3->destroy(string3);
}
