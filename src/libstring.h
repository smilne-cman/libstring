#ifndef __libstring__
#define __libstring__

/* Includes *******************************************************************/

/* Types **********************************************************************/

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
int ends_with(const char *str, const char *suffix);
int starts_with(const char *pre, const char *str);

const char *lowercase(const char *string);
const char *uppercase(const char *string);

int string_index(const char *value, char character);
int string_last_index(const char *value, char character);

const char *substring(const char *value, int start, int end);

int string_to_bool(const char *value);

/* !!! DEPRECATED !!! *********************************************************/
char *trim(char *str);
char *split(const char *value, const char *delimiter);
const char *string_append(const char *first, const char *second);

#endif
