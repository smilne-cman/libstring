#ifndef __dynamic__
#define __dynamic__

/* Includes *******************************************************************/

/* Types **********************************************************************/
typedef struct DynamicString {
  int length;
  char *value;

  void (*assign)(struct DynamicString *self, const char *value);
  void (*trim)(struct DynamicString *self);
  void (*prepend)(struct DynamicString *self, const char *str);
  void (*append)(struct DynamicString *self, const char *str);

  int (*find)(struct DynamicString *self, const char *target);
  void (*replace)(struct DynamicString *self, const char *target, const char *value);
  void (*splice)(struct DynamicString *self, int start, int end, char *string);

  void (*lowercase)(struct DynamicString *self);
  void (*uppercase)(struct DynamicString *self);

  char *(*to_string)(struct DynamicString *self);
  char *(*substring)(struct DynamicString *self, int start, int end);
  int (*equals)(struct DynamicString *self, struct DynamicString *other);

  void (*destroy)(struct DynamicString *self);
} DynamicString;

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
DynamicString *dynamic_string(const char *value);

#endif
