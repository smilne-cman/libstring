#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "libstring.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static int equals_either(const char *value, const char *first, const char *second);

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
char *trim(char *str) {
  char *end;
  /* skip leading whitespace */
  while (isspace(*str)) {
      str = str + 1;
  }
  /* remove trailing whitespace */
  end = str + strlen(str) - 1;
  while (end > str && isspace(*end)) {
      end = end - 1;
  }
  /* write null character */
  *(end+1) = '\0';
  return str;
}

char *split(const char *value, const char *delimiter) {
  char *result = (char *)malloc(sizeof(char) * 1024);
  strcpy(result, value);

  return strtok(result, delimiter);
}

int ends_with(const char *str, const char *suffix) {
  int str_len = strlen(str);
  int suffix_len = strlen(suffix);

  return
    (str_len >= suffix_len) &&
    (0 == strcmp(str + (str_len-suffix_len), suffix));
}

int starts_with(const char *pre, const char *str) {
  size_t lenpre = strlen(pre);
  size_t lenstr = strlen(str);

  return lenstr < lenpre ? 0 : memcmp(pre, str, lenpre) == 0;
}

const char *lowercase(const char *string) {
  char *result = (char *)malloc(sizeof(char) * strlen(string));

  for (int i = 0; string[i]; i++) {
    result[i] = tolower(string[i]);
  }

  return result;
}

const char *uppercase(const char *string) {
  char *result = (char *)malloc(sizeof(char) * strlen(string));

  for (int i = 0; string[i]; i++) {
    result[i] = toupper(string[i]);
  }

  return result;
}

int string_index(const char *value, char character) {
  char *pointer = strchr(value, character);

  if (pointer ==  NULL) {
    return -1;
  }

  return pointer - value;
}

int string_last_index(const char *value, char character) {
  int pointer = strlen(value) - 1;

  while(pointer > 0) {
    if (character == value[pointer]) {
      return pointer;
    }

    pointer--;
  }

  return -1;
}

const char *string_append(const char *first, const char *second) {
  char *result = (char *)malloc(strlen(first) + strlen(second));

  sprintf(result, "%s%s", first, second);

  return result;
}

const char *substring(const char *value, int start, int end) {
  int initialLength = strlen(value);
  if (end == -1) {
    end = initialLength - 1;
  }

  if (start > end) {
    return "";
  }

  int length = end - start + 2;

  if (start > initialLength || end > initialLength) {
    return "";
  }

  char *result = (char *)malloc(sizeof(char) * length);

  int offset = start;
  int pointer = 0;
  while(offset <= end) {
    result[pointer] = value[offset];

    pointer++;
    offset++;
  }

	result[pointer] = NULL;

  return result;
}

int string_to_bool(const char *value) {
  const char *lowered = lowercase(value);

  if (
    !strcmp(lowered, "true") ||
    !strcmp(lowered, "yes") ||
    !strcmp(lowered, "on") ||
    !strcmp(lowered, "y") ||
    !strcmp(lowered, "1")
  ) {
    return 1;
  }

  return 0;
}

static int equals_either(const char *value, const char *first, const char *second) {
  return !strcmp(value, first) || !strcmp(value, second);
}
