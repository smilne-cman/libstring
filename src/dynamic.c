#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dynamic.h"
#include "libstring.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static void dynamic_assign(DynamicString *self, const char *value);
static void dynamic_trim(DynamicString *self);
static void dynamic_prepend(DynamicString *self, const char *str);
static void dynamic_append(DynamicString *self, const char *str);
static int dynamic_find(DynamicString *self, const char *target);
static void dynamic_replace(DynamicString *self, const char *target, const char *value);
static void dynamic_splice(DynamicString *self, int start, int end, char *string);
static void dynamic_lowercase(DynamicString *self);
static void dynamic_uppercase(DynamicString *self);
static char *dynamic_to_string(DynamicString *self);
static char *dynamic_substring(DynamicString *self, int start, int end);
static int dynamic_equals(DynamicString *self, DynamicString *other);
static void dynamic_destroy(DynamicString *self);

static int is_valid_index(DynamicString *self, int index);

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/

DynamicString *dynamic_string(const char *value) {
  DynamicString *instance = (DynamicString *)malloc(sizeof(DynamicString));

  instance->length = 0;
  instance->value = NULL;

  instance->assign = dynamic_assign;
  instance->trim = dynamic_trim;
  instance->prepend = dynamic_prepend;
  instance->append = dynamic_append;
  instance->find = dynamic_find;
  instance->replace = dynamic_replace;
  instance->splice  = dynamic_splice;
  instance->lowercase = dynamic_lowercase;
  instance->uppercase = dynamic_uppercase;
  instance->to_string = dynamic_to_string;
  instance->equals = dynamic_equals;
  instance->destroy = dynamic_destroy;

  instance->assign(instance, value);

  return instance;
}

static void dynamic_assign(DynamicString *self, const char *value) {
  if (self->value != NULL) {
    free(self->value);
  }
 
  self->length = strlen(value);

  char *clone = (char *)malloc(sizeof(char) * self->length + 1);

  strcpy(clone, value);
  self->value = clone;
}

static void dynamic_trim(DynamicString *self) {
  char *clone = self->to_string(self);

  char *trimmed = trim(clone);

  self->assign(self, trimmed);
  free(clone);
}

static void dynamic_prepend(DynamicString *self, const char *str) {
  int length = self->length + strlen(str);
  char *combined = (char *)malloc(sizeof(char) * length);

  sprintf(combined, "%s%s", str, self->value);

  self->assign(self, combined);
  free(combined);
}

static void dynamic_append(DynamicString *self, const char *str) {
  int length = self->length + strlen(str);
  char *combined = (char *)malloc(sizeof(char) * length);

  sprintf(combined, "%s%s", self->value, str);

  self->assign(self, combined);
  free(combined);
}

static int dynamic_find(DynamicString *self, const char *target) {
  int target_length = strlen(target);

  for(int i = 0; i < self->length; i++) {
    // If there isn't enough room to find the string then exit
    if (self->length - i + 1 < target_length) return -1;

    if (self->value[i] == target[0]) {
      for (int j = 1; j < target_length; j++) {
        // If characters don't match then stop walking target
        if (self->value[i + j] != target[j]) break;

        // If we have walked over the whole target then we must have found a match
        if (j == target_length - 1) return i;
      }
    }
  }

  return -1;
}

static void dynamic_replace(DynamicString *self, const char *target, const char *value) {
  int index = self->find(self, target);
  int target_length = strlen(target);

  if (index == -1) return;

  self->splice(self, index, index + target_length, value);
}

static void dynamic_splice(struct DynamicString *self, int start, int end, char *string) {
  if (!is_valid_index(self, start) || !is_valid_index(self, end)) return;

  char *first = self->to_string(self);
  char *last = self->to_string(self);

  // Snip the end off the start and the start off the end 
  first[start] = '\0';
  char *last_offset = last + end;

  if (string == NULL) {
    string = "";
  }

  int new_length = strlen(first) + strlen(string) + strlen(last_offset);
  char *working = (char *)malloc(sizeof(char) * new_length);

  sprintf(working, "%s%s%s", first, string, last_offset);

  self->assign(self, working);

  free(working);
  free(first);
  free(last);
}

static void dynamic_lowercase(DynamicString *self) {
  self->assign(self, lowercase(self->value));
}

static void dynamic_uppercase(DynamicString *self) {
  self->assign(self, uppercase(self->value));
}

static char *dynamic_to_string(DynamicString *self) {
  char *clone = (char *)malloc(sizeof(char) * self->length);

  strcpy(clone, self->value);
  return clone;
}

static char *dynamic_substring(DynamicString *self, int start, int end) {
  return substring(self->value, start, end);
}

static int dynamic_equals(DynamicString *self, DynamicString *other) {
  return !strcmp(self->value, other->value);
}

static void dynamic_destroy(DynamicString *self) {
  if (self->value != NULL) {
    free(self->value);
  }

  free(self);
}

static int is_valid_index(DynamicString *self, int index) {
  return index >= 0 && index <= self->length;
}